# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :tia,
  ecto_repos: [Tia.Repo]

# Configures the endpoint
config :tia, TiaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "B0yrLzalA0i+kHYicyaEb09JDgkF0GJyWQ84UlDx2QDacSzsFQf9SdA53dDX8TKW",
  render_errors: [view: TiaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Tia.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]


# Configure Guardian
config :guardian, Guardian,
  issuer: "Psicocalia.Portaria.Server",
  ttl: { 1, :days },
  secret_key: "2btAVMiQVeTdJX0bMmRM0q8nOi9HHB8XDb5IkdSRlrimdbGXT43ABa7+9wKMsJ0i", 
  serializer: Tia.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
