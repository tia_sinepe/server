defmodule Tia.Repo.Migrations.CreateStageclasses do
  use Ecto.Migration

  def change do
    create table(:stageclasses) do
      add :class_id, references(:classes, on_delete: :nothing)
      add :stage_id, references(:stages, on_delete: :nothing)

      timestamps()
    end

    create index(:stageclasses, [:class_id])
    create index(:stageclasses, [:stage_id])
  end
end
