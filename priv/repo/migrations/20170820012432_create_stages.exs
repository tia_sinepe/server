defmodule Tia.Repo.Migrations.CreateStages do
  use Ecto.Migration

  def change do
    create table(:stages) do
      add :name, :string
      add :background, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :type_id, references(:types, on_delete: :nothing)

      timestamps()
    end

    create index(:stages, [:user_id])
    create index(:stages, [:type_id])
  end
end
