defmodule Tia.GameTest do
  use Tia.DataCase

  alias Tia.Game

  describe "types" do
    alias Tia.Game.Type

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def type_fixture(attrs \\ %{}) do
      {:ok, type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Game.create_type()

      type
    end

    test "list_types/0 returns all types" do
      type = type_fixture()
      assert Game.list_types() == [type]
    end

    test "get_type!/1 returns the type with given id" do
      type = type_fixture()
      assert Game.get_type!(type.id) == type
    end

    test "create_type/1 with valid data creates a type" do
      assert {:ok, %Type{} = type} = Game.create_type(@valid_attrs)
      assert type.name == "some name"
    end

    test "create_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Game.create_type(@invalid_attrs)
    end

    test "update_type/2 with valid data updates the type" do
      type = type_fixture()
      assert {:ok, type} = Game.update_type(type, @update_attrs)
      assert %Type{} = type
      assert type.name == "some updated name"
    end

    test "update_type/2 with invalid data returns error changeset" do
      type = type_fixture()
      assert {:error, %Ecto.Changeset{}} = Game.update_type(type, @invalid_attrs)
      assert type == Game.get_type!(type.id)
    end

    test "delete_type/1 deletes the type" do
      type = type_fixture()
      assert {:ok, %Type{}} = Game.delete_type(type)
      assert_raise Ecto.NoResultsError, fn -> Game.get_type!(type.id) end
    end

    test "change_type/1 returns a type changeset" do
      type = type_fixture()
      assert %Ecto.Changeset{} = Game.change_type(type)
    end
  end

  describe "stages" do
    alias Tia.Game.Stage

    @valid_attrs %{background: "some background", name: "some name"}
    @update_attrs %{background: "some updated background", name: "some updated name"}
    @invalid_attrs %{background: nil, name: nil}

    def stage_fixture(attrs \\ %{}) do
      {:ok, stage} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Game.create_stage()

      stage
    end

    test "list_stages/0 returns all stages" do
      stage = stage_fixture()
      assert Game.list_stages() == [stage]
    end

    test "get_stage!/1 returns the stage with given id" do
      stage = stage_fixture()
      assert Game.get_stage!(stage.id) == stage
    end

    test "create_stage/1 with valid data creates a stage" do
      assert {:ok, %Stage{} = stage} = Game.create_stage(@valid_attrs)
      assert stage.background == "some background"
      assert stage.name == "some name"
    end

    test "create_stage/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Game.create_stage(@invalid_attrs)
    end

    test "update_stage/2 with valid data updates the stage" do
      stage = stage_fixture()
      assert {:ok, stage} = Game.update_stage(stage, @update_attrs)
      assert %Stage{} = stage
      assert stage.background == "some updated background"
      assert stage.name == "some updated name"
    end

    test "update_stage/2 with invalid data returns error changeset" do
      stage = stage_fixture()
      assert {:error, %Ecto.Changeset{}} = Game.update_stage(stage, @invalid_attrs)
      assert stage == Game.get_stage!(stage.id)
    end

    test "delete_stage/1 deletes the stage" do
      stage = stage_fixture()
      assert {:ok, %Stage{}} = Game.delete_stage(stage)
      assert_raise Ecto.NoResultsError, fn -> Game.get_stage!(stage.id) end
    end

    test "change_stage/1 returns a stage changeset" do
      stage = stage_fixture()
      assert %Ecto.Changeset{} = Game.change_stage(stage)
    end
  end

  describe "classes" do
    alias Tia.Game.Class

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def class_fixture(attrs \\ %{}) do
      {:ok, class} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Game.create_class()

      class
    end

    test "list_classes/0 returns all classes" do
      class = class_fixture()
      assert Game.list_classes() == [class]
    end

    test "get_class!/1 returns the class with given id" do
      class = class_fixture()
      assert Game.get_class!(class.id) == class
    end

    test "create_class/1 with valid data creates a class" do
      assert {:ok, %Class{} = class} = Game.create_class(@valid_attrs)
      assert class.name == "some name"
    end

    test "create_class/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Game.create_class(@invalid_attrs)
    end

    test "update_class/2 with valid data updates the class" do
      class = class_fixture()
      assert {:ok, class} = Game.update_class(class, @update_attrs)
      assert %Class{} = class
      assert class.name == "some updated name"
    end

    test "update_class/2 with invalid data returns error changeset" do
      class = class_fixture()
      assert {:error, %Ecto.Changeset{}} = Game.update_class(class, @invalid_attrs)
      assert class == Game.get_class!(class.id)
    end

    test "delete_class/1 deletes the class" do
      class = class_fixture()
      assert {:ok, %Class{}} = Game.delete_class(class)
      assert_raise Ecto.NoResultsError, fn -> Game.get_class!(class.id) end
    end

    test "change_class/1 returns a class changeset" do
      class = class_fixture()
      assert %Ecto.Changeset{} = Game.change_class(class)
    end
  end

  describe "stageclasses" do
    alias Tia.Game.StageClass

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def stage_class_fixture(attrs \\ %{}) do
      {:ok, stage_class} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Game.create_stage_class()

      stage_class
    end

    test "list_stageclasses/0 returns all stageclasses" do
      stage_class = stage_class_fixture()
      assert Game.list_stageclasses() == [stage_class]
    end

    test "get_stage_class!/1 returns the stage_class with given id" do
      stage_class = stage_class_fixture()
      assert Game.get_stage_class!(stage_class.id) == stage_class
    end

    test "create_stage_class/1 with valid data creates a stage_class" do
      assert {:ok, %StageClass{} = stage_class} = Game.create_stage_class(@valid_attrs)
    end

    test "create_stage_class/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Game.create_stage_class(@invalid_attrs)
    end

    test "update_stage_class/2 with valid data updates the stage_class" do
      stage_class = stage_class_fixture()
      assert {:ok, stage_class} = Game.update_stage_class(stage_class, @update_attrs)
      assert %StageClass{} = stage_class
    end

    test "update_stage_class/2 with invalid data returns error changeset" do
      stage_class = stage_class_fixture()
      assert {:error, %Ecto.Changeset{}} = Game.update_stage_class(stage_class, @invalid_attrs)
      assert stage_class == Game.get_stage_class!(stage_class.id)
    end

    test "delete_stage_class/1 deletes the stage_class" do
      stage_class = stage_class_fixture()
      assert {:ok, %StageClass{}} = Game.delete_stage_class(stage_class)
      assert_raise Ecto.NoResultsError, fn -> Game.get_stage_class!(stage_class.id) end
    end

    test "change_stage_class/1 returns a stage_class changeset" do
      stage_class = stage_class_fixture()
      assert %Ecto.Changeset{} = Game.change_stage_class(stage_class)
    end
  end
end
