defmodule TiaWeb.StageClassControllerTest do
  use TiaWeb.ConnCase

  alias Tia.Game
  alias Tia.Game.StageClass

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:stage_class) do
    {:ok, stage_class} = Game.create_stage_class(@create_attrs)
    stage_class
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all stageclasses", %{conn: conn} do
      conn = get conn, stage_class_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create stage_class" do
    test "renders stage_class when data is valid", %{conn: conn} do
      conn = post conn, stage_class_path(conn, :create), stage_class: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, stage_class_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, stage_class_path(conn, :create), stage_class: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update stage_class" do
    setup [:create_stage_class]

    test "renders stage_class when data is valid", %{conn: conn, stage_class: %StageClass{id: id} = stage_class} do
      conn = put conn, stage_class_path(conn, :update, stage_class), stage_class: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, stage_class_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn, stage_class: stage_class} do
      conn = put conn, stage_class_path(conn, :update, stage_class), stage_class: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete stage_class" do
    setup [:create_stage_class]

    test "deletes chosen stage_class", %{conn: conn, stage_class: stage_class} do
      conn = delete conn, stage_class_path(conn, :delete, stage_class)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, stage_class_path(conn, :show, stage_class)
      end
    end
  end

  defp create_stage_class(_) do
    stage_class = fixture(:stage_class)
    {:ok, stage_class: stage_class}
  end
end
