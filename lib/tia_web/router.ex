defmodule TiaWeb.Router do
  use TiaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
  end

  scope "/", TiaWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", TiaWeb do
     pipe_through :api

     resources "/users", UserController, expert: [:new, :edit]
     resources "/types", TypeController, except: [:new, :edit]
     resources "/stages", StageController, except: [:new, :edit]
     resources "/classes", ClassController, except: [:new, :edit]
     resources "/stageclasses", StageClassController, except: [:new, :edit]

     post "/login", SessionController, :create
   end
end
