defmodule TiaWeb.PageController do
  use TiaWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
