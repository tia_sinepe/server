defmodule TiaWeb.TypeController do
  use TiaWeb, :controller

  alias Tia.Game
  alias Tia.Game.Type

  plug Guardian.Plug.EnsureAuthenticated, [handler: TiaWeb.GuardianErrorHandler]  when action in [:create, :update, :delete] 
  plug :scrub_params, "type" when action in [:create, :update]

  action_fallback TiaWeb.FallbackController

  def index(conn, _params) do
    types = Game.list_types()
    render(conn, "index.json", types: types)
  end

  def create(conn, %{"type" => type_params}) do
    with {:ok, %Type{} = type} <- Game.create_type(type_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", type_path(conn, :show, type))
      |> render("show.json", type: type)
    end
  end

  def show(conn, %{"id" => id}) do
    type = Game.get_type!(id)
    render(conn, "show.json", type: type)
  end

  def update(conn, %{"id" => id, "type" => type_params}) do
    type = Game.get_type!(id)

    with {:ok, %Type{} = type} <- Game.update_type(type, type_params) do
      render(conn, "show.json", type: type)
    end
  end

  def delete(conn, %{"id" => id}) do
    type = Game.get_type!(id)
    with {:ok, %Type{}} <- Game.delete_type(type) do
      send_resp(conn, :no_content, "")
    end
  end
end
