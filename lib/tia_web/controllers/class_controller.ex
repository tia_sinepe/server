defmodule TiaWeb.ClassController do
  use TiaWeb, :controller

  alias Tia.Game
  alias Tia.Game.Class

  action_fallback TiaWeb.FallbackController

  def index(conn, _params) do
    classes = Game.list_classes()
    render(conn, "index.json", classes: classes)
  end

  def create(conn, %{"class" => class_params}) do
    with {:ok, %Class{} = class} <- Game.create_class(class_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", class_path(conn, :show, class))
      |> render("show.json", class: class)
    end
  end

  def show(conn, %{"id" => id}) do
    class = Game.get_class!(id)
    render(conn, "show.json", class: class)
  end

  def update(conn, %{"id" => id, "class" => class_params}) do
    class = Game.get_class!(id)

    with {:ok, %Class{} = class} <- Game.update_class(class, class_params) do
      render(conn, "show.json", class: class)
    end
  end

  def delete(conn, %{"id" => id}) do
    class = Game.get_class!(id)
    with {:ok, %Class{}} <- Game.delete_class(class) do
      send_resp(conn, :no_content, "")
    end
  end
end
