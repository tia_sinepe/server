defmodule TiaWeb.GuardianErrorHandler do
  use TiaWeb, :controller
  alias TiaWeb.GuardianErrorView
  def unauthenticated(conn, _params) do
      conn
      |> put_status(:forbidden)
      |> render(GuardianErrorView, :forbidden)
    end
end