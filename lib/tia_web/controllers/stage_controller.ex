defmodule TiaWeb.StageController do
  use TiaWeb, :controller

  alias Tia.Game
  alias Tia.Game.Stage

  action_fallback TiaWeb.FallbackController

  def index(conn, _params) do
    stages = Game.list_stages()
    render(conn, "index.json", stages: stages)
  end

  def create(conn, %{"stage" => stage_params}) do
    with {:ok, %Stage{} = stage} <- Game.create_stage(stage_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", stage_path(conn, :show, stage))
      |> render("show.json", stage: stage)
    end
  end

  def show(conn, %{"id" => id}) do
    stage = Game.get_stage!(id)
    render(conn, "show.json", stage: stage)
  end

  def update(conn, %{"id" => id, "stage" => stage_params}) do
    stage = Game.get_stage!(id)

    with {:ok, %Stage{} = stage} <- Game.update_stage(stage, stage_params) do
      render(conn, "show.json", stage: stage)
    end
  end

  def delete(conn, %{"id" => id}) do
    stage = Game.get_stage!(id)
    with {:ok, %Stage{}} <- Game.delete_stage(stage) do
      send_resp(conn, :no_content, "")
    end
  end
end
