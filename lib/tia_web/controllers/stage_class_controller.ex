defmodule TiaWeb.StageClassController do
  use TiaWeb, :controller

  alias Tia.Game
  alias Tia.Game.StageClass

  action_fallback TiaWeb.FallbackController

  def index(conn, _params) do
    stageclasses = Game.list_stageclasses()
    render(conn, "index.json", stageclasses: stageclasses)
  end

  def create(conn, %{"stage_class" => stage_class_params}) do
    with {:ok, %StageClass{} = stage_class} <- Game.create_stage_class(stage_class_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", stage_class_path(conn, :show, stage_class))
      |> render("show.json", stage_class: stage_class)
    end
  end

  def show(conn, %{"id" => id}) do
    stage_class = Game.get_stage_class!(id)
    render(conn, "show.json", stage_class: stage_class)
  end

  def update(conn, %{"id" => id, "stage_class" => stage_class_params}) do
    stage_class = Game.get_stage_class!(id)

    with {:ok, %StageClass{} = stage_class} <- Game.update_stage_class(stage_class, stage_class_params) do
      render(conn, "show.json", stage_class: stage_class)
    end
  end

  def delete(conn, %{"id" => id}) do
    stage_class = Game.get_stage_class!(id)
    with {:ok, %StageClass{}} <- Game.delete_stage_class(stage_class) do
      send_resp(conn, :no_content, "")
    end
  end
end
