defmodule TiaWeb.GuardianErrorView do
  use TiaWeb, :view
  def render("forbidden.json", _assigns) do
    %{message: "Forbidden"}
  end
end