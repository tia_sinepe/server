defmodule TiaWeb.StageClassView do
  use TiaWeb, :view
  alias TiaWeb.StageClassView

  def render("index.json", %{stageclasses: stageclasses}) do
    %{data: render_many(stageclasses, StageClassView, "stage_class.json")}
  end

  def render("show.json", %{stage_class: stage_class}) do
    %{data: render_one(stage_class, StageClassView, "stage_class.json")}
  end

  def render("stage_class.json", %{stage_class: stage_class}) do
    %{id: stage_class.id}
  end
end
