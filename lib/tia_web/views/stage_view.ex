defmodule TiaWeb.StageView do
  use TiaWeb, :view
  alias TiaWeb.StageView

  def render("index.json", %{stages: stages}) do
    %{data: render_many(stages, StageView, "stage.json")}
  end

  def render("show.json", %{stage: stage}) do
    %{data: render_one(stage, StageView, "stage.json")}
  end

  def render("stage.json", %{stage: stage}) do
    %{id: stage.id,
      name: stage.name,
      background: stage.background}
  end
end
