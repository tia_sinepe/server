defmodule TiaWeb.SessionView do
  use TiaWeb, :view

  def render("login.json", %{jwt: jwt}) do
    %{ jwt: jwt }
  end
end