defmodule Tia.Account.User do
  use Ecto.Schema
  import Ecto.Queryable
  import Ecto.Changeset
  import Ecto.Query
  alias Tia.Account.User
  alias Tia.Repo

  schema "users" do
    field :email, :string
    field :password_digest, :string
    field :password, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_required([:email, :password])
    |> put_pass_hash
  end

  defp put_pass_hash(changeset) do
    password = changeset.changes.password
    put_change(changeset, :password_digest, Comeonin.Bcrypt.hashpwsalt(password))
  end

  def find_by_email(email) do
    query =
      from u in User,
      where: u.email == ^email
  end
end
