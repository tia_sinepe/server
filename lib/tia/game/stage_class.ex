defmodule Tia.Game.StageClass do
  use Ecto.Schema
  import Ecto.Changeset
  alias Tia.Game.StageClass


  schema "stageclasses" do
    field :class_id, :id
    field :stage_id, :id

    timestamps()
  end

  @doc false
  def changeset(%StageClass{} = stage_class, attrs) do
    stage_class
    |> cast(attrs, [])
    |> validate_required([])
  end
end
