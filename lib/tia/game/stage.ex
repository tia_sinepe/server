defmodule Tia.Game.Stage do
  use Ecto.Schema
  import Ecto.Changeset
  alias Tia.Game.Stage

  require IEx

  schema "stages" do
    field :background, :string
    field :name, :string
    field :user_id, :id
    field :type_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Stage{} = stage, attrs) do
    stage
    |> cast(attrs, [:name, :background])
    |> validate_required([:name, :background])
  end
end
