defmodule Tia.Game.Class do
  use Ecto.Schema
  import Ecto.Changeset
  alias Tia.Game.Class


  schema "classes" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(%Class{} = class, attrs) do
    class
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
