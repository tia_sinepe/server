defmodule Tia.Game do
  @moduledoc """
  The Game context.
  """

  import Ecto.Query, warn: false
  alias Tia.Repo

  alias Tia.Game.Type

  @doc """
  Returns the list of types.

  ## Examples

      iex> list_types()
      [%Type{}, ...]

  """
  def list_types do
    Repo.all(Type)
  end

  @doc """
  Gets a single type.

  Raises `Ecto.NoResultsError` if the Type does not exist.

  ## Examples

      iex> get_type!(123)
      %Type{}

      iex> get_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_type!(id), do: Repo.get!(Type, id)

  @doc """
  Creates a type.

  ## Examples

      iex> create_type(%{field: value})
      {:ok, %Type{}}

      iex> create_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_type(attrs \\ %{}) do
    %Type{}
    |> Type.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a type.

  ## Examples

      iex> update_type(type, %{field: new_value})
      {:ok, %Type{}}

      iex> update_type(type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_type(%Type{} = type, attrs) do
    type
    |> Type.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Type.

  ## Examples

      iex> delete_type(type)
      {:ok, %Type{}}

      iex> delete_type(type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_type(%Type{} = type) do
    Repo.delete(type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking type changes.

  ## Examples

      iex> change_type(type)
      %Ecto.Changeset{source: %Type{}}

  """
  def change_type(%Type{} = type) do
    Type.changeset(type, %{})
  end

  alias Tia.Game.Stage

  @doc """
  Returns the list of stages.

  ## Examples

      iex> list_stages()
      [%Stage{}, ...]

  """
  def list_stages do
    Repo.all(Stage)
  end

  @doc """
  Gets a single stage.

  Raises `Ecto.NoResultsError` if the Stage does not exist.

  ## Examples

      iex> get_stage!(123)
      %Stage{}

      iex> get_stage!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stage!(id), do: Repo.get!(Stage, id)

  @doc """
  Creates a stage.

  ## Examples

      iex> create_stage(%{field: value})
      {:ok, %Stage{}}

      iex> create_stage(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stage(attrs \\ %{}) do
    %Stage{}
    |> Stage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stage.

  ## Examples

      iex> update_stage(stage, %{field: new_value})
      {:ok, %Stage{}}

      iex> update_stage(stage, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stage(%Stage{} = stage, attrs) do
    stage
    |> Stage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Stage.

  ## Examples

      iex> delete_stage(stage)
      {:ok, %Stage{}}

      iex> delete_stage(stage)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stage(%Stage{} = stage) do
    Repo.delete(stage)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stage changes.

  ## Examples

      iex> change_stage(stage)
      %Ecto.Changeset{source: %Stage{}}

  """
  def change_stage(%Stage{} = stage) do
    Stage.changeset(stage, %{})
  end

  alias Tia.Game.Class

  @doc """
  Returns the list of classes.

  ## Examples

      iex> list_classes()
      [%Class{}, ...]

  """
  def list_classes do
    Repo.all(Class)
  end

  @doc """
  Gets a single class.

  Raises `Ecto.NoResultsError` if the Class does not exist.

  ## Examples

      iex> get_class!(123)
      %Class{}

      iex> get_class!(456)
      ** (Ecto.NoResultsError)

  """
  def get_class!(id), do: Repo.get!(Class, id)

  @doc """
  Creates a class.

  ## Examples

      iex> create_class(%{field: value})
      {:ok, %Class{}}

      iex> create_class(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_class(attrs \\ %{}) do
    %Class{}
    |> Class.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a class.

  ## Examples

      iex> update_class(class, %{field: new_value})
      {:ok, %Class{}}

      iex> update_class(class, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_class(%Class{} = class, attrs) do
    class
    |> Class.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Class.

  ## Examples

      iex> delete_class(class)
      {:ok, %Class{}}

      iex> delete_class(class)
      {:error, %Ecto.Changeset{}}

  """
  def delete_class(%Class{} = class) do
    Repo.delete(class)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking class changes.

  ## Examples

      iex> change_class(class)
      %Ecto.Changeset{source: %Class{}}

  """
  def change_class(%Class{} = class) do
    Class.changeset(class, %{})
  end

  alias Tia.Game.StageClass

  @doc """
  Returns the list of stageclasses.

  ## Examples

      iex> list_stageclasses()
      [%StageClass{}, ...]

  """
  def list_stageclasses do
    Repo.all(StageClass)
  end

  @doc """
  Gets a single stage_class.

  Raises `Ecto.NoResultsError` if the Stage class does not exist.

  ## Examples

      iex> get_stage_class!(123)
      %StageClass{}

      iex> get_stage_class!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stage_class!(id), do: Repo.get!(StageClass, id)

  @doc """
  Creates a stage_class.

  ## Examples

      iex> create_stage_class(%{field: value})
      {:ok, %StageClass{}}

      iex> create_stage_class(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stage_class(attrs \\ %{}) do
    %StageClass{}
    |> StageClass.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stage_class.

  ## Examples

      iex> update_stage_class(stage_class, %{field: new_value})
      {:ok, %StageClass{}}

      iex> update_stage_class(stage_class, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stage_class(%StageClass{} = stage_class, attrs) do
    stage_class
    |> StageClass.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a StageClass.

  ## Examples

      iex> delete_stage_class(stage_class)
      {:ok, %StageClass{}}

      iex> delete_stage_class(stage_class)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stage_class(%StageClass{} = stage_class) do
    Repo.delete(stage_class)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stage_class changes.

  ## Examples

      iex> change_stage_class(stage_class)
      %Ecto.Changeset{source: %StageClass{}}

  """
  def change_stage_class(%StageClass{} = stage_class) do
    StageClass.changeset(stage_class, %{})
  end
end
